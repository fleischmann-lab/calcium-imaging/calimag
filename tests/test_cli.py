import shutil
from pathlib import Path
from unittest.mock import patch

import calimag as calimag
import pytest
import test_constants as TEST_CST
import toml
from calimag.cli import cli
from calimag.config import GetConfig
from calimag.constants import EXPERIMENT_VERSIONS, TEENSY_HEADER
from click.testing import CliRunner
from filelock import FileLock
from pynwb import NWBHDF5IO
from test_utils import ContextLock

# @pytest.mark.parametrize(
#     "empty_fields_input, continue_input, empty_fields",
#     [
#         ("doesn't matter", "y", False),
#         ("n", "doesn't matter", False),
#         ("garbage", "garbage", False),
#         ("Y", "Y", False),
#         ("N", "doesn't matter", False),
#         ("y", "n", True),
#         ("y", "N", True),
#         ("y", "y", True),
#         ("n", "doesn't matter", True),
#     ],
# )
# def test_cli(empty_fields_input, continue_input, empty_fields):
#     if empty_fields:
#         config_filepath = TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["config"]
#     else:
#         config_filepath = TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"]
#     config = GetConfig(config_filepath)
#     nwb_test_path = config.get("DataInput").get("nwb_output_filepath")
#     lock_path = Path(str(nwb_test_path) + ".lock")
#     lock = FileLock(lock_path, timeout=1)
#     lock.acquire(timeout=30)
#     if nwb_test_path.exists():
#         nwb_test_path.unlink()

#     # Mock the returned NWB file to make testing faster
#     if continue_input.lower() == "y":
#         mocked_nwb_path = TEST_CST.FILEPATHS["singles"]["nwb"]
#         shutil.copy(mocked_nwb_path, nwb_test_path)

#     def return_nwb_if_y():
#         if continue_input.lower() == "y":
#             with NWBHDF5IO(str(nwb_test_path), "r") as io:
#                 mocked_nwb = io.read()
#             return mocked_nwb
#         else:
#             return False

#     if empty_fields:
#         input_mock = f"{empty_fields_input}\n{continue_input}\n"
#     else:
#         input_mock = f"{continue_input}\n"

#     with patch(
#         "calimag.nwb_converter.NwbConverter.Convert2NWB",
#         side_effect=return_nwb_if_y,
#     ):
#         runner = CliRunner()
#         result = runner.invoke(cli, [str(config_filepath)], input=input_mock)

#     def assert_output(result):
#         assert (
#             "Experiment version: "
#             f"{config.get('DataInput').get('experiment_version')}" in result.output
#         )
#         assert "Using the following configuration file:\n"
#         f"{str(config_filepath.absolute())}" in result.output
#         assert "The resulting NWB file will be written at the following location:\n"
#         f"{str(nwb_test_path.absolute())}" in result.output
#         assert "Aborted!" not in result.output

#     try:
#         assert result.exit_code == 0
#         assert f"Calimag version {calimag.__version__}" in result.output

#         if empty_fields:
#             assert "The following fields in the config are empty:" in result.output
#             if empty_fields_input.lower() == "y":
#                 if continue_input.lower() == "y":
#                     assert_output(result)
#                 else:
#                     assert "Aborted!" in result.output
#             else:
#                 assert "Aborted!" in result.output
#         else:
#             if continue_input.lower() == "y":
#                 assert_output(result)
#             else:
#                 assert "Aborted!" in result.output

#     finally:
#         if nwb_test_path.exists():
#             nwb_test_path.unlink()
#         assert not nwb_test_path.exists()
#         lock.release()
#         lock_path.unlink()
#         assert not lock_path.exists()
#         GetConfig.cache_clear()


@pytest.mark.parametrize(
    "continue_input",
    [
        "y",
        "doesn't matter",
        "garbage",
        "Y",
        "n",
        "N",
    ],
)
def test_cli(continue_input):
    config_filepath = TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"]
    config = GetConfig(config_filepath)
    nwb_test_path = config.get("DataInput").get("nwb_output_filepath")
    lock_path = Path(str(nwb_test_path) + ".lock")
    lock = FileLock(lock_path, timeout=1)
    lock.acquire(timeout=30)
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    # Mock the returned NWB file to make testing faster
    if continue_input.lower() == "y":
        mocked_nwb_path = TEST_CST.FILEPATHS["singles"]["nwb"]
        shutil.copy(mocked_nwb_path, nwb_test_path)

    def return_nwb_if_y(skip_validation):
        if continue_input.lower() == "y":
            with NWBHDF5IO(str(nwb_test_path), "r") as io:
                mocked_nwb = io.read()
            return mocked_nwb
        else:
            return False

    input_mock = f"{continue_input}\n"

    with patch(
        "calimag.nwb_converter.NwbConverter.Convert2NWB",
        side_effect=return_nwb_if_y,
    ):
        runner = CliRunner()
        result = runner.invoke(cli, [str(config_filepath)], input=input_mock)

    def assert_output(result):
        assert (
            "Experiment version: "
            f"{config.get('DataInput').get('experiment_version')}" in result.output
        )
        assert (
            "Using the following configuration file:\n"
            f"{str(config_filepath.absolute())}" in result.output
        )
        assert (
            "The resulting NWB file will be written at the following location:\n"
            f"{str(nwb_test_path.absolute())}" in result.output
        )
        assert "Aborted!" not in result.output

    try:
        assert result.exit_code == 0
        assert f"Calimag version {calimag.__version__}" in result.output

        if continue_input.lower() == "y":
            assert_output(result)
        else:
            assert "Aborted!" in result.output

    finally:
        if nwb_test_path.exists():
            nwb_test_path.unlink()
        assert not nwb_test_path.exists()
        lock.release()
        lock_path.unlink()
        assert not lock_path.exists()
        GetConfig.cache_clear()


@pytest.mark.parametrize(
    "input_config",
    [
        """
[DataInput]
    microscope_filepath = "./data/v2021_05-2p-Simon/TSeries-05112021-1455-001_trim.xml"
    teensy_filepath = '''
            ./data/v2021_05-2p-Simon/20210511_Mouse#513_Teensy_serial_data_trim.csv'''
    suite2pNpy_directory = "./data/v2021_05-2p-Simon/suite2p_trim"
    nwb_output_filepath = "./data/v2021_05-2p-Simon/output.nwb"

[NWBFile]
    session_description = "Test calimag with 2 channels"
        """,
        """
[NWBFile]
    session_description = "Test calimag with 2 channels"
        """,
        """
[DataInput]
    experiment_version = "v2021.05:2p_imaging_head_fixed+Simon"
    microscope_filepath = "./data/v2021_05-2p-Simon/TSeries-05112021-1455-001_trim.xml"
    teensy_filepath = '''
            ./data/v2021_05-2p-Simon/20210511_Mouse#513_Teensy_serial_data_trim.csv'''
    suite2pNpy_directory = "./data/v2021_05-2p-Simon/suite2p_trim"
    nwb_output_filepath = "./data/v2021_05-2p-Simon/output.nwb"

[NWBFile]
        """,
        """
[DataInput]
    experiment_version = "v2019.07:2p_imaging_head_fixed+Max"
    microscope_filepath = "./data/v2019_07-2p-Max/TSeries-02042020-1448-001_trim.xml"
    teensy_filepath = "./data/v2019_07-2p-Max/serialdata_m8_trim.txt"
    suite2pNpy_directory = "./data/v2019_07-2p-Max/suite2p_trim"
    nwb_output_filepath = "./data/v2019_07-2p-Max/output.nwb"

[NWBFile]
    session_description = "My awesome session"

[Imaging]
    Device.Type = "Microscope"
    OpticalChannelDescription = "GCaMP/Green, tdTomado/red"
    emission_lambda = 520
    CalciumIndicator = "jGCaMP7f"
    ImagingLocation = "PCX"
        """,
        """
[DataInput]
    experiment_version = "v2019.07:2p_imaging_head_fixed+Max"
    microscope_filepath = "./data/v2019_07-2p-Max/TSeries-02042020-1448-001_trim.xml"
    teensy_filepath = "./data/v2019_07-2p-Max/serialdata_m8_trim.txt"
    suite2pNpy_directory = "./data/v2019_07-2p-Max/suite2p_trim"
    nwb_output_filepath = "./data/v2019_07-2p-Max/output.nwb"

[NWBFile]
    session_description = "My awesome session"

[Imaging]
    Device.Type = "Microscope"
    OpticalChannelDescription = "GCaMP/Green, tdTomado/red"
    emission_lambda = 520
    PlaneDescription = "My awesome imaging plane"
    CalciumIndicator = "jGCaMP7f"
    ImagingLocation = "PCX"
        """,
        """
[DataInput]
    experiment_version = "v2019.07:2p_imaging_head_fixed+Max"
    microscope_filepath = "./data/v2019_07-2p-Max/TSeries-02042020-1448-001_trim.xml"
    teensy_filepath = "./data/v2019_07-2p-Max/serialdata_m8_trim.txt"
    suite2pNpy_directory = "./data/v2019_07-2p-Max/suite2p_trim"
    nwb_output_filepath = "./data/v2019_07-2p-Max/output.nwb"

[NWBFile]
    session_description = "My awesome session"
    lab = "Fleischmann Lab"
    institution = "Brown University"
    timezone = "EST"
    experimenter = ""
    experiment_description = ""

[Subject]
    age = "73w (ended)"
    description = "Mouse #8"
    genotype = "tdTomato +/-"
    sex = "male"
    species = "mouse"
    subject_id = 8
    weight = 31 # [grams]
    date_of_birth = 2019-02-18

[Imaging]
    Device.Type = "Microscope"
    Device.Description = "Ultima Investigator"
    Device.Manufacturer = "Bruker"
    OpticalChannelDescription = "GCaMP/Green, tdTomado/red"
    emission_lambda = 520
    PlaneDescription = "My awesome imaging plane"
    CalciumIndicator = "jGCaMP7f"
    ImagingLocation = "PCX"
    grid_spacing = [2.0, 2.0, 30.0]
    grid_spacing_unit = "microns"
    reference_frame = ""
        """,
    ],
    ids=[
        "Missing `experiment_version`",
        "Missing `[DataInput]`",
        "Missing 1 mandatory field",
        "Missing several mandatory fields",
        "Missing only optional fields and user decides to abort",
        "Not missing anything but user decides to abort",
    ],
)
def test_abort_cases(input_config):
    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=input_config):
        runner = CliRunner()
        result = runner.invoke(cli, [str(config_filepath)], input="n\n")
        assert "Aborted!" in result.output

        # Need to clear the cache otherwise the function isn't called
        GetConfig.cache_clear()


def test_version_cli():
    runner = CliRunner()
    result = runner.invoke(cli, ["--version"])
    assert result.exit_code == 0
    assert calimag.__version__ in result.output


@pytest.mark.parametrize(
    "cli_arg, missing_optional_fields",
    [("-y", False), ("-y", True), ("--yes", False), ("--yes", True)],
)
def test_cli_auto_yes(cli_arg, missing_optional_fields):
    if missing_optional_fields:
        config_filepath = TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["config"]
    else:
        config_filepath = TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"]

    config = GetConfig(config_filepath)
    nwb_test_path = config.get("DataInput").get("nwb_output_filepath")
    lock_path = Path(str(nwb_test_path) + ".lock")
    lock = FileLock(lock_path, timeout=1)
    lock.acquire(timeout=30)
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    # Mock the returned NWB file to make testing faster
    mocked_nwb_path = TEST_CST.FILEPATHS["singles"]["nwb"]
    shutil.copy(mocked_nwb_path, nwb_test_path)

    def return_mocked_nwb(skip_validation):
        with NWBHDF5IO(str(nwb_test_path), "r") as io:
            mocked_nwb = io.read()
        return mocked_nwb

    with patch(
        "calimag.nwb_converter.NwbConverter.Convert2NWB",
        side_effect=return_mocked_nwb,
    ):
        runner = CliRunner()
        result = runner.invoke(cli, [cli_arg, str(config_filepath)])

    try:
        assert result.exit_code == 0
        assert "Continue anyway? [y/n]" not in result.output
        assert "Aborted!" not in result.output
        assert "Done!" in result.output

        missing_msg = "The following fields in the config are empty"
        if missing_optional_fields:
            assert missing_msg in result.output
        else:
            assert missing_msg not in result.output

    finally:
        if nwb_test_path.exists():
            nwb_test_path.unlink()
        assert not nwb_test_path.exists()
        lock.release()
        lock_path.unlink()
        assert not lock_path.exists()
        GetConfig.cache_clear()


@pytest.mark.parametrize(
    "cli_arg",
    ["-l", "--list-versions"],
)
def test_list_experiment_versions(cli_arg):
    runner = CliRunner()
    result = runner.invoke(cli, [cli_arg])

    assert result.exit_code == 0
    assert all([x in result.output for x in EXPERIMENT_VERSIONS])
    assert "calimag -E" in result.output


@pytest.mark.parametrize(
    "cli_arg, exp_ver, expected_error",
    [
        ("-E", EXPERIMENT_VERSIONS[0], False),
        ("--describe-version", EXPERIMENT_VERSIONS[1], False),
        ("-E", "IMAGINARY_VERSION", True),
        ("--describe-version", "IMAGINARY_VERSION", True),
    ],
)
def test_describe_experiment_versions(cli_arg, exp_ver, expected_error):
    runner = CliRunner()
    result = runner.invoke(cli, [cli_arg, exp_ver])
    exit_code = result.exit_code
    cli_out = result.output

    # if errors
    if expected_error:
        assert exit_code == 2
        return

    # no errors
    assert exit_code == 0
    assert exp_ver in cli_out
    assert calimag.__version__ in cli_out

    expected_teensy_cfg = {
        x["name"]: {k: v for k, v in x.items() if k != "name"}
        for x in TEENSY_HEADER[exp_ver]
    }

    expected_teensy_cfg_toml = toml.dumps(expected_teensy_cfg)

    assert expected_teensy_cfg_toml in cli_out
