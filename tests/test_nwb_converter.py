import itertools
import re
import time
import timeit
from itertools import combinations
from pathlib import Path
from unittest.mock import patch

import calimag.errors as err
import numpy as np
import pandas as pd
import pytest
import test_constants as TEST_CST
import toml
from calimag.config import GetConfig
from calimag.constants import TEENSY_HEADER
from calimag.nwb_converter import NwbConverter
from pynwb import NWBHDF5IO, NWBFile, TimeSeries
from pynwb.epoch import TimeIntervals
from test_utils import ContextLock


def test_ProcessTimeSeries():
    dir_alias = "v2019_07-2p-Max"
    exp_ver = "v2019.07:2p_imaging_head_fixed+Max"
    data_paths = TEST_CST.FILEPATHS[dir_alias]
    conv = NwbConverter(config_filepath=data_paths["config"])
    data_ts = conv.ProcessTimeSeries(
        teensy_filepath=data_paths["teensy"],
        microscope_filepath=data_paths["xml"],
    )
    teensy_header = (
        pd.DataFrame(TEENSY_HEADER[exp_ver]).set_index("name").to_dict("index")
    )

    for name in data_ts.acquisition:
        container = data_ts.acquisition.get(name)
        assert isinstance(container, TimeSeries)
        if name == "flow":
            assert container.unit == "m3/sec"
            assert container.description == teensy_header["flow"]["description"]
        elif name == "wheel":
            assert container.unit == "-"
            assert container.description == teensy_header["wheel"]["description"]
            assert container.comments == teensy_header["wheel"]["comments"]

    for name in data_ts.stimulus:
        container = data_ts.stimulus.get(name)
        assert isinstance(container, TimeSeries)
        if "odor" in name:
            assert container.unit == "-"
            assert container.description == teensy_header["odor"]["description"]
            assert container.comments == teensy_header["odor"]["comments"]

    assert isinstance(data_ts.trials, TimeIntervals)
    GetConfig.cache_clear()


# def test_ProcessImagingData():
#     conv = NwbConverter(
#         session_description=SESSION_DESCRIPTION,
#         microscope_filepath=XML_FILEPATH[4],
#         teensy_filepath=TEENSY_FILEPATHS[4],
#         suite2pNpyPath=str(SUITE2PNPYPATH),
#         TwoPhotonSeriesUrlOrPath=TWOPHOTONSERIES,
#     )
#     imgfile = conv.ProcessImagingData(
#         xml_filepath=XML_FILEPATH[4],
#         nwb2p_filepath=NWB2P,
#         TwoPhotonSeriesUrlOrPath=TWOPHOTONSERIES,
#     )
#     assert imgfile


# def test_Convert2NWB():
#     nwb_test_path = DATA_PATH.joinpath(
#         "test.nwb",
#     )
#     if nwb_test_path.exists():
#         nwb_test_path.unlink()

#     conv = NwbConverter(
#         session_description=SESSION_DESCRIPTION,
#         microscope_filepath=str(XML_FILEPATH[4]),
#         teensy_filepath=str(TEENSY_FILEPATHS[4]),
#         segmentation_fp=str(NWB2P),
#         TwoPhotonSeriesUrlOrPath=TWOPHOTONSERIES,
#     )
#     nwb = conv.Convert2NWB(nwb_output_path=str(nwb_test_path))
#     assert isinstance(nwb, NWBFile)

#     # Rename segmentation file to check that there are no links between files
#     NWB2P_backup = NWB2P
#     NWB2P.rename("dummy.test")

#     with NWBHDF5IO(str(nwb_test_path), "r") as io:
#         read_nwbfile = io.read()
#         assert read_nwbfile.acquisition
#         assert read_nwbfile.stimulus
#         assert read_nwbfile.trials
#         assert read_nwbfile.processing
#         assert read_nwbfile.processing["ophys"].data_interfaces["Deconvolved"]
#         assert read_nwbfile.processing["ophys"].data_interfaces["Fluorescence"]
#         assert read_nwbfile.processing["ophys"].data_interfaces["Neuropil"]
#     # Revert and clean
#     NWB2P.rename(NWB2P_backup)
#     nwb_test_path.unlink()


def test_H5IO():
    test_data = dict()
    for condition, path_dict in TEST_CST.H5IO_TEST_FILEPATHS.items():
        cfg_path = path_dict["config"]
        config = toml.load(path_dict["config"])
        nwb_path = Path(config["DataInput"]["nwb_output_filepath"])
        if nwb_path.exists():
            nwb_path.unlink()

        elapsed_dict = dict()

        # convert + measure time
        t0 = time.time()
        conv = NwbConverter(config_filepath=cfg_path)
        nwb = conv.Convert2NWB()
        elapsed_dict["conv"] = time.time() - t0
        file_size_MB = nwb_path.stat().st_size / 1e6

        # metadata assertions
        assert isinstance(nwb, NWBFile)
        assert nwb.identifier == "ace47c20624973e1ac0215782504d770"
        assert nwb.session_start_time.isoformat() == "2020-02-04T18:01:38.714876-05:00"

        # reload and read some data for later
        t0 = time.time()
        data_dict = dict()
        with NWBHDF5IO(str(nwb_path), "r") as io:
            # read
            read_nwbfile = io.read()
            elapsed_dict["read"] = (t_read := time.time()) - t0

            # get odor
            data_dict["odor_time"] = read_nwbfile.stimulus["odor"].timestamps[:]
            data_dict["odor_vals"] = read_nwbfile.stimulus["odor"].data[:]
            elapsed_dict["get_odor"] = (t_odor := time.time()) - t_read

            # get flow
            data_dict["flow_time"] = read_nwbfile.acquisition["flow"].timestamps[:]
            data_dict["flow_vals"] = read_nwbfile.acquisition["flow"].data[:]
            elapsed_dict["get_flow"] = time.time() - t_odor

            # get an arbitrary cells of Fluorescence
            fluo_mod = read_nwbfile.processing["ophys"][
                "Fluorescence"
            ].roi_response_series
            elapsed_dict["get_subset_fluo"] = timeit.timeit(
                lambda fluo_mod=fluo_mod: fluo_mod["Plane_0"].data[:, 10:20],
                number=10000,
            )

            # get all fluorescence data
            t0_fluo = time.time()
            for k in ["Plane_0", "Plane_1", "Plane_2"]:
                data_dict[k + "_data"] = fluo_mod[k].data[:]
                data_dict[k + "_time"] = fluo_mod[k].timestamps[:]
            elapsed_dict["get_fluo"] = time.time() - t0_fluo

        elapsed_dict["total"] = time.time() - t0

        # unlink and clear cache
        nwb_path.unlink()
        GetConfig.cache_clear()

        # save some
        test_data[condition] = dict(
            file_size_MB=file_size_MB, elapsed=elapsed_dict, data=data_dict
        )

    # just print times
    print(pd.DataFrame({k: v["elapsed"] for k, v in test_data.items()}).to_markdown())

    # check file size
    # note chunk is not guaranteed to be same as disable
    assert test_data["explicit"]["file_size_MB"] < min(
        test_data["chunkonly"]["file_size_MB"], test_data["disable"]["file_size_MB"]
    )

    # compare datafields
    data_fields = list(data_dict.keys())
    conditions = list(test_data.keys())
    for k in data_fields:
        for c1, c2 in combinations(conditions, 2):
            v1 = test_data[c1]["data"][k]
            v2 = test_data[c2]["data"][k]
            assert np.array_equal(
                v1, v2
            ), f"Data not equal at `{k}` for H5IO conditions `{c1}` and `{c2}`"


def test_Convert2NWBfromNpy_main():
    config = toml.load(TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"])
    nwb_test_path = Path(config.get("DataInput").get("nwb_output_filepath"))
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    conv = NwbConverter(config_filepath=TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"])
    nwb = conv.Convert2NWB()
    assert isinstance(nwb, NWBFile)

    with NWBHDF5IO(str(nwb_test_path), "r") as io:
        read_nwbfile = io.read()
        assert read_nwbfile.acquisition
        assert read_nwbfile.stimulus
        assert read_nwbfile.trials
        assert read_nwbfile.subject
        assert read_nwbfile.processing
        ophys = read_nwbfile.processing["ophys"]
        assert ophys.data_interfaces["Deconvolved"]
        Fluorescence = ophys.data_interfaces["Fluorescence"]
        assert Fluorescence
        assert ophys.data_interfaces["Neuropil"]
        assert Fluorescence.roi_response_series["Plane_0"].data[:].shape == (1360, 142)
        assert Fluorescence.roi_response_series["Plane_1"].data[:].shape == (1360, 74)
        assert Fluorescence.roi_response_series["Plane_2"].data[:].shape == (1360, 99)
        assert Fluorescence.roi_response_series["Plane_0"].timestamps.shape == (1360,)
        assert Fluorescence.roi_response_series["Plane_1"].timestamps.shape == (1360,)
        assert Fluorescence.roi_response_series["Plane_2"].timestamps.shape == (1360,)

        background_keys = [
            x for x in ophys.data_interfaces if "backgrounds" in x.lower()
        ]
        num_backgrounds = len(background_keys)
        assert num_backgrounds == 3  # redundant but ok
        for dat_k in [
            "Deconvolved",
            "Fluorescence",
            "Fluorescence_chan2",
            "Neuropil",
            "Neuropil_chan2",
        ]:
            if dat_k not in ophys.data_interfaces:
                continue
            assert (_roi_series := ophys[dat_k].roi_response_series)
            assert num_backgrounds == len(_roi_series.keys())
        images_keys = ["Vcorr", "max_proj", "meanImg"]
        for img_k in ophys[background_keys[0]].images:
            assert img_k in images_keys
            img_set = []
            for bg_k in background_keys:
                assert (_im := ophys[bg_k][img_k])
                img_set.append(_im.data[:])
            for i, j in itertools.combinations(range(len(img_set)), 2):
                assert not np.array_equal(img_set[i], img_set[j])

        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_0"].rois[:].index.to_numpy(),
            np.arange(0, 142),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_1"].rois[:].index.to_numpy(),
            np.arange(142, 216),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_2"].rois[:].index.to_numpy(),
            np.arange(216, 315),
        )

        # Check that the timestamps from the neural + behavioral data
        # start at the same time
        global_ref_time = pd.Timestamp(read_nwbfile.timestamps_reference_time)
        assert global_ref_time.isoformat() == "2020-02-04T18:01:38.714876-05:00"
        roi_response_series = ophys.data_interfaces["Fluorescence"].roi_response_series
        flow = read_nwbfile.acquisition["flow"]
        assert (
            global_ref_time
            + pd.Timedelta(seconds=roi_response_series["Plane_0"].timestamps[0])
        ).isoformat() == "2020-02-04T18:01:38.714876-05:00"
        assert (
            global_ref_time + pd.Timedelta(seconds=flow.timestamps[0])
        ).isoformat() == "2020-02-04T18:01:38.714876-05:00"

        trial_start = read_nwbfile.trials["start_time"][:]
        trial_end = read_nwbfile.trials["stop_time"][:]
        trial_start_expected = np.array([0.0, 60.006, 120.003, 180.005, 240.005])
        trial_end_expected = np.array([29.999, 90.005, 150.002, 210.004, 270.004])
        np.testing.assert_array_equal(
            trial_start[0:5],
            trial_start_expected,
        )
        np.testing.assert_array_equal(
            trial_end[0:5],
            trial_end_expected,
        )

    nwb_test_path.unlink()
    GetConfig.cache_clear()


def test_NwbConverter_no_subject():
    with patch("toml.load") as mocked_config:
        mocked_config.return_value = {
            "DataInput": {
                "microscope_filepath": "./data/v2019_07-2p-Max/"
                "TSeries-02042020-1448-001_trim.xml",
                "teensy_filepath": "./data/v2019_07-2p-Max/serialdata_m8_trim.txt",
                "suite2pNpy_directory": "./data/v2019_07-2p-Max/suite2p_trim",
                "nwb_output_filepath": "./data/v2019_07-2p-Max/test.nwb",
            },
            "NWBFile": {
                "session_description": "My awesome session",
                "lab": "Fleischmann Lab",
                "institution": "Brown University",
                "timezone": "EST",
            },
            "Imaging": {
                "Device": {"Type": "Microscope"},
                "PlaneDescription": "My awesome imaging plane",
            },
        }
        conv = NwbConverter(
            config_filepath=TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"]
        )
        assert conv.nwbfile
        assert conv.config.get("Subject") is None
        GetConfig.cache_clear()


def test_Convert2NWBfromNpy_chan2():
    config = toml.load(TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["config"])
    nwb_test_path = Path(config.get("DataInput").get("nwb_output_filepath"))
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    conv = NwbConverter(
        config_filepath=TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["config"]
    )
    nwb = conv.Convert2NWB()
    assert isinstance(nwb, NWBFile)

    with NWBHDF5IO(str(nwb_test_path), "r") as io:
        read_nwbfile = io.read()
        assert read_nwbfile.acquisition
        assert read_nwbfile.stimulus
        assert read_nwbfile.processing
        ophys = read_nwbfile.processing["ophys"]
        assert ophys.data_interfaces["Deconvolved"]
        Fluorescence = ophys.data_interfaces["Fluorescence"]
        assert Fluorescence

        background_keys = [
            x for x in ophys.data_interfaces if "backgrounds" in x.lower()
        ]
        num_backgrounds = len(background_keys)
        assert num_backgrounds == 3  # redundant but ok
        for dat_k in [
            "Deconvolved",
            "Fluorescence",
            "Fluorescence_chan2",
            "Neuropil",
            "Neuropil_chan2",
        ]:
            if dat_k not in ophys.data_interfaces:
                continue
            assert (_roi_series := ophys[dat_k].roi_response_series)
            assert num_backgrounds == len(_roi_series.keys())
        images_keys = ["Vcorr", "max_proj", "meanImg", "meanImg_chan2"]
        for img_k in ophys[background_keys[0]].images:
            assert img_k in images_keys
            img_set = []
            for bg_k in background_keys:
                assert (_im := ophys[bg_k][img_k])
                img_set.append(_im.data[:])
            for i, j in itertools.combinations(range(len(img_set)), 2):
                assert not np.array_equal(img_set[i], img_set[j])

        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_0"].rois[:].index.to_numpy(),
            np.arange(0, 207),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_1"].rois[:].index.to_numpy(),
            np.arange(207, 530),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_2"].rois[:].index.to_numpy(),
            np.arange(530, 669),
        )
        Fluorescence_chan2 = ophys.data_interfaces["Fluorescence_chan2"]
        assert Fluorescence_chan2
        np.testing.assert_array_equal(
            Fluorescence_chan2.roi_response_series["Plane_0"].rois[:].index.to_numpy(),
            np.arange(0, 207),
        )
        np.testing.assert_array_equal(
            Fluorescence_chan2.roi_response_series["Plane_1"].rois[:].index.to_numpy(),
            np.arange(207, 530),
        )
        np.testing.assert_array_equal(
            Fluorescence_chan2.roi_response_series["Plane_2"].rois[:].index.to_numpy(),
            np.arange(530, 669),
        )
        assert ophys.data_interfaces["Neuropil"]
        assert ophys.data_interfaces["Neuropil_chan2"]
        global_ref_time = pd.Timestamp(read_nwbfile.timestamps_reference_time)
        assert global_ref_time.isoformat() == "2021-05-11T14:57:25.952809-05:00"

    nwb_test_path.unlink()
    GetConfig.cache_clear()


@pytest.mark.parametrize(
    "skip_validation",
    [
        True,
        False,
    ],
)
def test_NWBvalidation(skip_validation):
    config_filepath = TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"]
    config = toml.load(config_filepath)
    nwb_test_path = Path(config.get("DataInput").get("nwb_output_filepath"))
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    with config_filepath.open("r") as fid:
        file_content = fid.read()
    nwbfile_content = """
[Subject]
    age = "99 years"
    sex = "Prefer not say"
    species = "Alien"
"""
    pattern = re.compile(r"(?sm)(\[Subject\])(.*?)(^\s*$)")
    file_content_new = pattern.sub(nwbfile_content, file_content)
    assert file_content_new != file_content
    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=file_content_new):
        config = GetConfig(config_filepath=config_filepath)

        conv = NwbConverter(config_filepath=config_filepath)
        if not skip_validation:
            with pytest.raises(err.CriticalErrorDuringValidation):
                conv.Convert2NWB(skip_validation=skip_validation)
            (
                nwb_test_path.parent / f"nwbinspector_report_{nwb_test_path.name}.log"
            ).unlink()
        else:
            assert conv.Convert2NWB(skip_validation=skip_validation)

    nwb_test_path.unlink()
    GetConfig.cache_clear()
