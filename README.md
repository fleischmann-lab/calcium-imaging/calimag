# Calimag

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/fleischmann-lab/calcium-imaging/calimag?branch=master&label=pipeline&style=flat-square)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/commits/master)
[![coverage report](https://img.shields.io/gitlab/pipeline-coverage/fleischmann-lab/calcium-imaging/calimag?branch=master&job_name=test&style=flat-square)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/commits/master)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

[![conda package](https://img.shields.io/conda/v/fleischmannlab/calimag?color=blue&style=flat-square)](https://anaconda.org/FleischmannLab/calimag)
[![license](https://img.shields.io/gitlab/license/fleischmann-lab/calcium-imaging/calimag?color=yellow&label=license&style=flat-square)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/LICENSE)

CLI tool to convert behavior & calcium imaging data from the Fleischmann's lab (Brown University) to the NWB standard file format.

## Table of Contents

<!-- - [Background](#background) -->

- [Overview](#overview)
- [Install](#install)
- [Usage](#usage)
- [Development](#development)

<!-- ## Background -->

## Overview

Here's a video demonstrating how to use the tool from the command li

![Demo video](docs/demo.ogv)

The resulting NWB file will look like the following:

![NWB output](docs/nwb-output.png)

You can use [HDFView](https://www.hdfgroup.org/downloads/hdfview/) to easily browse inside the NWB file. The NWB file produced should also be easily shareable on [DANDI](https://dandiarchive.org/) without modifications.

## Setup

### Prerequisites

You need to have the `Conda` package manager installed. You can get it from the [Anaconda](https://www.anaconda.com/products/individual) or the [Miniconda](https://docs.conda.io/en/latest/miniconda.html) distributions.

### Install

To install the package, you can optionally create a new isolated environment for it and activate this environment:

```bash
conda create -n calimag python=3.10
conda activate calimag
```

Then to install it, run the following commands:

```bash
conda install -c conda-forge -c fleischmannlab calimag
```

### Update

To update the package to the latest version, first activate the right environment, for example:

```bash
conda activate calimag
```

Then run the following command:

```bash
conda update calimag
```

The different versions are listed [here](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/tags).

## Usage

To list options, please use the following command:

```bash
calimag --help
```

### Configuration

Download [this configuration file](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/tests/user.config.toml) to your local machine and replace the file paths and the metadata by your own values.

### Experiment version

The allowed experiment versions labels are defined in the [`constants.py` file at the `EXPERIMENT_VERSIONS` key](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/src/calimag/constants.py).

To list allowed versions:

```bash
calimag -l
```

To get the metadata of Teensy columns of a specified version, here's an example:

```bash
calimag -E 'v2019.07:2p_imaging_head_fixed+Max'
```

### Convert your files to NWB

Run the following command in your terminal:

```bash
calimag "/path/to/your/config/file.toml"
```

This will prompt you for confirmation of information and will inform you if your configuration is valid to proceed.

If you want to always accept _yes_ automatically, use the `-y` tag. For example:

```bash
calimag -y "/path/to/your/config/file.toml"
```
