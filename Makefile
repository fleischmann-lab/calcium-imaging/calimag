##
# Calimag
#
# @file
# @version 0.1

include .env.test

help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

black-check:
	black --check .

black-format:
	black .

ruff-check:
	ruff check


ruff-format:
	ruff check --fix
	ruff format

mypy:
	mypy -p calimag --lineprecision-report .
	cat lineprecision.txt

codespell:
	codespell

prettier-check:
	prettier --check "**/*.{yaml,yml}" && \
	prettier --check "**/*.md"

prettier-format:
	prettier --write "**/*.{yaml,yml}"
	prettier --write "**/*.md"

format: ## Autoformat everything
	make ruff-format
	make black-format
	make prettier-format

lint-python: ## Run Python based static analysis
	make ruff-check && \
	make black-check && \
	make mypy && \
	make codespell

lint-all: ## Run all static analysis
	make lint-python && \
	make prettier-check

get-data: ## Get data from GIN
	test -d data && ls -l data
	datalad clone https://gin.g-node.org/fleischmann-lab/calimag-testdata data
	cd data
	git pull
	git checkout $(GIN_DATALAD_CHECKOUT)
	git pull
	datalad get . -J 4
	cd ..

ci-install-datalad: ## Install datalad on CI
	apt-get install -y git-annex
	datalad --version
	git-annex version
	git config --global user.name "Gitlab CI runner"
	git config --global user.email "gitlab-bot@bot.noreply.gitlab.com"

ci-get-data: ## Get data on CI
	make ci-install-datalad
	make get-data
	du -hL data

test: ## Run tests
	pytest --cov=calimag -n auto tests/ --durations=10 -v

test-xml: ## Run tests with XML coverage
	pytest --cov=calimag --cov-append tests/ --durations=10 -v
	coverage report
	coverage xml

test-html: ## Run tests with HTML report
	pytest --cov=calimag --cov-report html -n auto tests/ --durations=10 -v

clean: ## Clean tmp files
	find . -type f -name *.pyc -delete
	find . -type d -name __pycache__ -delete

guix-shell: ## Spawn a isolated shell
	guix shell -m manifest.scm --pure

.ONESHELL:
conda-pkg: ## Build Conda package
	conda mambabuild --token $$ANACONDA_TOKEN --user $$ANACONDA_USER --channel conda-forge recipe/

# end
