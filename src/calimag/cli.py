"""Command Line Interface to run the software."""

from pathlib import Path

import click
import toml

import calimag as calimag
import calimag.constants as CONST


@click.command()
@click.version_option(version=calimag.__version__)
@click.argument(
    "configfile",
    type=click.Path(exists=True),
    required=False,
)
@click.option(
    "-y",
    "--yes",
    is_flag=True,
    show_default=True,
    default=False,
    help='Accept all actions to follow as "y"',
)
@click.option(
    "-l",
    "--list-versions",
    is_flag=True,
    default=False,
    help='List acceptable "EXPERIMENT_VERSION" in the config file',
)
@click.option(
    "-E",
    "--describe-version",
    nargs=1,
    type=click.Choice(CONST.EXPERIMENT_VERSIONS, case_sensitive=True),
    default=None,
    help='Describe a given "EXPERIMENT_VERSION"',
)
@click.option(
    "--skip-validation",
    is_flag=True,
    help="Skip the validation of the NWB file produced",
)
def cli(configfile, yes, list_versions, describe_version, skip_validation):
    """Run the main command-line function."""
    click.echo(f"\nCalimag version {calimag.__version__}\n")
    if list_versions:
        list_experiment_versions()
        return

    if describe_version is not None:
        describe_experiment_version(describe_version)
        return

    convert(configfile=configfile, auto_yes=yes, skip_validation=skip_validation)
    return


def list_experiment_versions():
    """List experimental versions."""
    click.echo("Currently these are the acceptable experiment versions:\n")
    click.echo("\n".join(["\t" + e for e in CONST.EXPERIMENT_VERSIONS]) + "\n")
    click.echo("For specific experiment version, use `-E`. For example:\n")
    click.echo(f"\tcalimag -E '{CONST.EXPERIMENT_VERSIONS[0]}'")
    click.echo(f"\tcalimag -E '{CONST.EXPERIMENT_VERSIONS[1]}'\n")


def describe_experiment_version(exp_version):
    """Describe experiment version."""
    import toml

    from calimag.constants import TEENSY_HEADER

    click.echo(f"The teensy config for {exp_version} is\n")

    teensy_cfg = TEENSY_HEADER.get(exp_version, None)
    # assert teensy_cfg is not None # not necessary, click already takes care
    teensy_cfg = {
        x["name"]: {k: v for k, v in x.items() if k != "name"} for x in teensy_cfg
    }
    teensy_cfg_toml = toml.dumps(teensy_cfg)
    click.echo(teensy_cfg_toml)


def convert(configfile, auto_yes=False, skip_validation=False):
    """Convert data to NWB file based on your CONFIGFILE."""
    click.echo(
        "\nUsing the following configuration file:\n"
        f"{click.format_filename(str(Path(configfile).absolute()))}"
    )

    from calimag.config import GetConfig, check_config_fields
    from calimag.nwb_converter import NwbConverter

    # Get the experiment_version
    config = toml.load(configfile)
    DataInput = config.get("DataInput")
    if DataInput is None:
        click.echo("Missing `[DataInput]` category in the config file")
        click.echo("Aborted!")
        return
    experiment_version = DataInput.get("experiment_version")
    if experiment_version is None:
        click.echo("Missing `experiment_version` field in the config file")
        click.echo("Aborted!")
        return

    # Handle missing fields in the config
    missing_fields = check_config_fields(
        config=config, config_ref=CONST.CONFIG_REFERENCE[experiment_version]
    )
    if len(missing_fields) > 0:
        # Raise an error if *mandatory* fields are missing
        if (missing_fields.mandatory).any():
            num_missing = len(missing_fields[missing_fields.mandatory])
            field_str = "fields are" if num_missing > 1 else "field is"

            click.echo(
                f"The following mandatory {field_str} missing from the config file:\n"
                f"{missing_fields.query('mandatory == True').path}"
            )
            click.echo("Aborted!")
            return

        else:  # Handle optional fields
            click.echo(
                f"\nThe following fields in the config are empty:\n{missing_fields}"
            )

            # Ask the user to continue or not
            user_choice = "y"
            if not auto_yes:
                user_choice = input("\nContinue anyway? [y/n] ")

            if user_choice.lower() != "y":
                click.echo("Aborted!")
                return

    config = GetConfig(configfile)
    click.echo(f"\nExperiment version: {experiment_version}")
    nwb_test_path = Path(DataInput.get("nwb_output_filepath"))
    click.echo(
        "\nThe resulting NWB file will be written at the following location:\n"
        f"{click.format_filename(str(nwb_test_path.absolute()))}"
    )

    # Ask the user to continue or not
    user_choice = "y"
    if not auto_yes:
        user_choice = input("\nContinue? [y/n] ")

    if user_choice.lower() == "y":
        # Run Calimag
        conv = NwbConverter(config_filepath=configfile)
        conv.Convert2NWB(skip_validation=skip_validation)
        click.echo("Done!")
    else:
        click.echo("Aborted!")
    return


if __name__ == "__main__":
    cli()
