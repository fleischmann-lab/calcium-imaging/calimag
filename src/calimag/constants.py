"""List of default constants."""

from typing import List

from calimag.utils import MandatoryField

TIMEZONE: str = "EST"
WHEEL_MAX_ENCODING: int = 4096
# WHEEL_MAX_RATE = 100

# Version pattern:
# vYYYY.MM:type-of-experiment[+optional-experimenter-name][:optional-label]
EXPERIMENT_VERSIONS: List[str] = [
    "v2021.05:2p_imaging_head_fixed+Simon",
    "v2019.07:2p_imaging_head_fixed+Max",
    "v2021.04:2p_imaging_head_fixed+Max:spontaneous-activity",
    "v2022.05:2p_imaging_head_fixed+Max",
    "v2020.06:2p_imaging_head_fixed+Simon",
]

# TODO: double-check the new "description" and "comments" fields
TEENSY_HEADER = {
    EXPERIMENT_VERSIONS[0]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "odor",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Odor stimulus ID (raw).",
            "comments": "This may not indicate when the stimulus is ON for each trial. "
            "This may not indicate the actual chemical names of the stimulus. "
            "Additional information may be needed to infer either.",
        },
        {
            "name": "unknown1",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "wheel",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Raw wheel position from micro-controller.",
            "comments": "Any value greater than "
            f"`WHEEL_MAX_ENCODING={WHEEL_MAX_ENCODING}` "
            "was turned into NaN values. "
            "Because this is from Teensy, there is no unit. "
            "To get wheel position in `radian` or speed in `meter/second`, "
            "additional information may be needed. "
            "However, for the former, for simplicity, one may cautiously "
            "consider `WHEEL_MAX_ENCODING` as `2 * pi`",
        },
        {"name": "trial", "unit": "-", "SI_unit": "-", "conversion_factor": 1},
        {
            "name": "unknown2",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
    ],
    EXPERIMENT_VERSIONS[1]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "odor",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Odor stimulus ID (raw).",
            "comments": "This may not indicate when the stimulus is ON for each trial. "
            "This may not indicate the actual chemical names of the stimulus. "
            "Additional information may be needed to infer either.",
        },
        {
            "name": "flow",
            "unit": "cm3/min",
            "SI_unit": "m3/sec",
            "conversion_factor": 60 * 1e-6,
            "description": "Air flow signal from sensor to measure respiration.",
        },
        {
            "name": "wheel",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Raw wheel position from micro-controller.",
            "comments": "Any value greater than "
            f"`WHEEL_MAX_ENCODING={WHEEL_MAX_ENCODING}` "
            "was turned into NaN values. "
            "Because this is from Teensy, there is no unit. "
            "To get wheel position in `radian` or speed in `meter/second`, "
            "additional information may be needed. "
            "However, for the former, for simplicity, one may cautiously "
            "consider `WHEEL_MAX_ENCODING` as `2 * pi`",
        },
        {
            "name": "trial",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
    ],
    EXPERIMENT_VERSIONS[2]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "flow",
            "unit": "cm3/min",
            "SI_unit": "m3/sec",
            "conversion_factor": 60 * 1e-6,
            "description": "Air flow signal from sensor to measure respiration.",
        },
        {
            "name": "wheel",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Raw wheel position from micro-controller.",
            "comments": "Any value greater than "
            f"`WHEEL_MAX_ENCODING={WHEEL_MAX_ENCODING}` "
            "was turned into NaN values. "
            "Because this is from Teensy, there is no unit. "
            "To get wheel position in `radian` or speed in `meter/second`, "
            "additional information may be needed. "
            "However, for the former, for simplicity, one may cautiously "
            "consider `WHEEL_MAX_ENCODING` as `2 * pi`",
        },
    ],
    EXPERIMENT_VERSIONS[3]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "odor",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Odor stimulus ID (raw).",
            "comments": "This may not indicate when the stimulus is ON for each trial. "
            "This may not indicate the actual chemical names of the stimulus. "
            "Additional information may be needed to infer either.",
        },
        {
            "name": "flow",
            "unit": "cm3/min",
            "SI_unit": "m3/sec",
            "conversion_factor": 60 * 1e-6,
            "description": "Air flow signal from sensor to measure respiration.",
        },
        {
            "name": "wheel",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Raw wheel position from micro-controller.",
            "comments": "Any value greater than "
            f"`WHEEL_MAX_ENCODING={WHEEL_MAX_ENCODING}` "
            "was turned into NaN values. "
            "Because this is from Teensy, there is no unit. "
            "To get wheel position in `radian` or speed in `meter/second`, "
            "additional information may be needed. "
            "However, for the former, for simplicity, one may cautiously "
            "consider `WHEEL_MAX_ENCODING` as `2 * pi`",
        },
        {
            "name": "odor_on",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Vector to indicate whether an odor is ON in a trial.",
        },
        {"name": "trial", "unit": "-", "SI_unit": "-", "conversion_factor": 1},
    ],
    EXPERIMENT_VERSIONS[4]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "odor",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Odor stimulus ID (raw).",
            "comments": "This may not indicate when the stimulus is ON for each trial. "
            "This may not indicate the actual chemical names of the stimulus. "
            "Additional information may be needed to infer either.",
        },
        {
            "name": "unknown1",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "wheel",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
            "description": "Raw wheel position from micro-controller.",
            "comments": "Any value greater than "
            f"`WHEEL_MAX_ENCODING={WHEEL_MAX_ENCODING}` "
            "was turned into NaN values. "
            "Because this is from Teensy, there is no unit. "
            "To get wheel position in `radian` or speed in `meter/second`, "
            "additional information may be needed. "
            "However, for the former, for simplicity, one may cautiously "
            "consider `WHEEL_MAX_ENCODING` as `2 * pi`",
        },
        {
            "name": "trial",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "unknown2",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
    ],
}


CONFIG_REFERENCE = {}
tmp_config_ref = {
    "DataInput": {
        "experiment_version": MandatoryField(True),
        "microscope_filepath": MandatoryField(True),
        "teensy_filepath": MandatoryField(True),
        "suite2pNpy_directory": MandatoryField(True),
        "nwb_output_filepath": MandatoryField(True),
    },
    "NWBFile": {
        "session_description": MandatoryField(True),
        "lab": MandatoryField(False),
        "institution": MandatoryField(False),
        "timezone": MandatoryField(False),
        "experimenter": MandatoryField(False),
        "experiment_description": MandatoryField(False),
    },
    "Subject": {
        "age": MandatoryField(False),
        "description": MandatoryField(False),
        "genotype": MandatoryField(False),
        "sex": MandatoryField(False),
        "species": MandatoryField(False),
        "subject_id": MandatoryField(False),
        "weight": MandatoryField(False),
        "date_of_birth": MandatoryField(False),
    },
    "Imaging": {
        "Device": {
            "Type": MandatoryField(True),
            "Description": MandatoryField(False),
            "Manufacturer": MandatoryField(False),
        },
        "OpticalChannelDescription": MandatoryField(True),
        "emission_lambda": MandatoryField(True),
        "PlaneDescription": MandatoryField(True),
        "CalciumIndicator": MandatoryField(True),
        "ImagingLocation": MandatoryField(True),
        "grid_spacing": MandatoryField(False),
        "grid_spacing_unit": MandatoryField(False),
        "reference_frame": MandatoryField(False),
    },
}
# Copy config ref for every `2p_imaging_head_fixed`
# experiment versions since they are all the same
for exp_ver in EXPERIMENT_VERSIONS:
    if "2p_imaging_head_fixed" in exp_ver:
        CONFIG_REFERENCE[exp_ver] = tmp_config_ref
del tmp_config_ref

MICROSCOPE_SUPPORTED_VERSIONS = {"5.4.64.700"}
SUPPORTED_XML_TYPES = {"ZSERIES", "TIMED"}

# This is currently a placeholder to allow more flexible change later
# TODO: allow option to change this in config file
FLUORESCENCE_CHANNEL_NAMES = {1: "green", 2: "red"}

# H5IO compression default configurations
DEFAULT_H5IO_CONFIG = dict(
    enable=True,
    chunks=True,
    compression="gzip",
    compression_opts=4,
)
