"""Utils functions and classes."""


class MandatoryField:
    """Object keeping track if a config field is mandatory."""

    def __init__(self, is_mandatory):
        if is_mandatory:
            self.is_mandatory = True
        else:
            self.is_mandatory = False

    def __repr__(self):
        if self.is_mandatory:
            return "<Mandatory>"
        else:
            return "<Not mandatory>"
