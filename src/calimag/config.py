"""Manage the configuration."""

from collections.abc import MutableMapping
from functools import lru_cache
from hashlib import md5
from pathlib import Path
from typing import Any, List

import pandas as pd
import toml

import calimag.constants as CONST
import calimag.errors as err
from calimag.parsers import MicroscopeParser
from calimag.utils import MandatoryField


@lru_cache(maxsize=None)  # Keep config in cache since function called several times
def GetConfig(config_filepath: Path) -> MutableMapping[str, Any]:
    """
    Parse config file.

    - Checks for missing values or undefined paths.
    - Adds some defaults values where needed.
    """
    config = toml.load(config_filepath)

    # DataInput
    DataInput = config.get("DataInput")
    if not DataInput:
        raise err.ConfigMissingItem("DataInput section in the config file is missing")
    for key, value in DataInput.items():
        # Check supported experiment version
        if key == "experiment_version":
            if value not in CONST.EXPERIMENT_VERSIONS:
                raise err.UnsupportedVersion(
                    f"The version `{value}` of your experiment "
                    "is unsupported, the currently supported versions are the "
                    f"following: {CONST.EXPERIMENT_VERSIONS}."
                )
            continue

        # Handle path-related config inputs
        # Convert paths strings to pathlib objects
        if "path" in (k := key.lower()) or "directory" in k:
            path = Path(value.strip())
            if key != "nwb_output_filepath":
                assert check_path_fields(key, path)

            config["DataInput"][key] = path

    # Subject
    Subject = config.get("Subject")
    if Subject:
        subject_new: dict = {}
        for key, item in Subject.items():
            # Skip if empty string
            if not item:
                continue
            # Only copy line from Dict config if filled in or not empty str
            if key == "subject_id":
                # NWB method needs a str
                subject_new["subject_id"] = str(item)
            elif key == "weight":
                # NWB method needs a str
                subject_new["weight"] = str(item)
            elif key == "date_of_birth":
                # NWB method needs a datetime object
                # Convert to datetime if time is not provided in the config
                NWBFileSection = config.get("NWBFile")
                if NWBFileSection:
                    timezone = NWBFileSection.get("timezone")
                    if timezone is None or timezone == "":
                        timezone = CONST.TIMEZONE
                    # TODO: Raise error if the date of birth field has a timezone?
                    subject_new["date_of_birth"] = pd.Timestamp(item).tz_localize(
                        timezone
                    )

            else:
                # Just copy every other field
                subject_new[key] = item

        config["Subject"] = subject_new

    # NWBFile
    # Inject other metadata
    microscope_filepath = config["DataInput"]["microscope_filepath"]
    parser = MicroscopeParser(filepath=microscope_filepath)
    session_start_time = parser.get_session_start_time()
    config["NWBFile"]["identifier"] = md5(
        str(session_start_time).encode("utf8")
    ).hexdigest()
    config["NWBFile"]["session_start_time"] = session_start_time

    # Imaging
    ImagingSection = config.get("Imaging")
    if not ImagingSection:
        raise err.ConfigMissingItem("Imaging section in the config file is missing")
    emission_lambda = ImagingSection.get("emission_lambda")
    if emission_lambda:
        config["Imaging"]["emission_lambda"] = float(emission_lambda)

    # h5io config: use `DEFAULT_H5IO_CONFIG` if not found
    # and turn certain keys with `False` to `None` as expected by H5IO
    h5io_cfg = config.get("H5IO", dict())
    false2none_keys = ["chunks", "compression"]
    for k, v in CONST.DEFAULT_H5IO_CONFIG.items():
        if k not in h5io_cfg:
            h5io_cfg[k] = v
        if k in false2none_keys and h5io_cfg[k] is False:
            h5io_cfg[k] = None

    # decide whether to enable
    enable_h5io = h5io_cfg.pop("enable", True)
    if not enable_h5io:
        h5io_cfg["chunks"] = None
        h5io_cfg["compression"] = None

    # remove compression options if no compress
    if h5io_cfg["compression"] is None:
        h5io_cfg.pop("compression_opts", None)

    config["H5IO"] = h5io_cfg

    return config


def check_path_fields(field: str, path: Path) -> bool:
    """
    Check path field satisfies convention (of only existing paths).

    Conventions:
    - Paths to specific files are signified by "*filepath*" in the field's name
    - Paths to directories are signified by "*directory*" in the field's name
    Returns True only if files exist and these 2 conditions are satisfied
    """
    field = field.lower()
    valid = False
    if "filepath" in field:
        valid = path.is_file()  # also checks for exists
        if not valid:
            raise FileNotFoundError(
                f"The path field '{field}' needs to be a path to an existing FILE."
                " Please check if this path exists and it is a file."
            )
    elif "directory" in field:
        valid = path.is_dir()  # also checks for exists
        if not valid:
            raise FileNotFoundError(
                f"The path field '{field}' needs to be a path to an existing DIRECTORY."
                " Please check if this path exists and it is a directory."
            )
    else:
        raise ValueError(f"The field to check {field} needs to be path-related inputs.")
    return valid


def remove_undefined_config_args(func_args: dict) -> dict:
    """
    Remove non mandatory config fields that are undefined.

    It should be safe to remove any undefined field as
    undefined mandatory fields are taken care of before.
    """
    new_args = func_args.copy()
    for key in func_args:
        if func_args.get(key) is None:
            new_args.pop(key)
    return new_args


def check_config_fields(config: dict, config_ref: dict) -> pd.DataFrame:
    """Check if the user forgot any empty fields in the TOML config."""
    missing_fields = pd.DataFrame()
    path_tree: List[str] = []

    def recurse_config_ref(
        node: dict,
        missing_fields: pd.DataFrame,
        path_tree: List[str],
        config_input: dict,
    ) -> pd.DataFrame:
        for key, item in node.items():
            path_tree.append(key)

            # If dict is a node
            if isinstance(item, dict):
                missing_fields = recurse_config_ref(
                    node=item,
                    missing_fields=missing_fields,
                    path_tree=path_tree,
                    config_input=config,
                )
                continue

            # If it's a leaf
            if isinstance(node.get(key), MandatoryField):
                # Test if those reference fields exist in the current config
                try:
                    for idx, elem in enumerate(path_tree):
                        if idx == 0:
                            config_field = config_input[elem]
                        else:
                            config_field = config_field[elem]
                except KeyError:
                    missing_fields = pd.concat(
                        [
                            missing_fields,
                            pd.DataFrame(
                                {
                                    "path": " > ".join(path_tree),
                                    "mandatory": item.is_mandatory,
                                },
                                index=[0],
                            ),
                        ],
                        ignore_index=True,
                    )
                path_tree.pop()  # Remove last leaf

            else:
                raise AttributeError("Config type error")

        if path_tree:
            path_tree.pop()  # Remove last node
        return missing_fields

    missing_fields = recurse_config_ref(
        node=config_ref,
        missing_fields=missing_fields,
        path_tree=path_tree,
        config_input=config,
    )
    return missing_fields
