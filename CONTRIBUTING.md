# Contributing

[TOC]

## Development

### Setup

To set up a development installation, first install the HDF5 binaries. The right HDF5 version to install is specified in the [`environment.yml`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/environment.yml) file. Then clone the repository and install the development dependencies:

```bash
git clone https://gitlab.com/fleischmann-lab/calcium-imaging/calimag.git
cd calimag
python -m venv .venv
source .venv/bin/activate
pip install -Ue .[dev]
```

### Data

#### Datalad installation

The test data are managed via `datalad` on GIN G-Node at <https://gin.g-node.org/fleischmann-lab/calimag-testdata>.

To set up, first install `git-annex` and `datalad`. See [the installation page](https://handbook.datalad.org/en/latest/intro/installation.html) for more information.

Additional `datalad` plugins can also be installed, but may conflict with `datalad` binary if it is installed via `apt` first.

```bash
pip install -e .[datalad]
```

#### Download dataset to local

Then to download run:

```bash
make get-data
```

This will also takes care of updating the dataset if the GIN G-Node remote has changes

#### Update dataset on GIN G-Node

This may still need some fine-tuning.

To be safe, **do it in another directory** instead of using this one.

##### Clone the GIN repository with data

To clone via SSH, you must first add your SSH key to <https://gin.g-node.org/user/settings/ssh>

```bash
# cd somewhere/else/not/the/gitlab/calimag
git clone git@gin.g-node.org:/fleischmann-lab/calimag-testdata.git
cd calimag-testdata
```

Please check first that the branch is `main`, and **not** `master` and the content is the same as the remote. If not, try removing that `calimag-testdata` and re-clone with:

```bash
git clone -b main git@gin.g-node.org:/fleischmann-lab/calimag-testdata.git
```

<details><summary>(Optional) Create a separate virtual environment</summary>

You need `datalad` to interact with this repository. If you prefer to have a separate environment, this GIN repository contains a `requirements.txt` that you can use to create one, for example:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

</details>

The repository you just clone only contains references to data, not the actual data, to obtain the data:

```bash
# use `-J 8` or `-J 10` for faster download if you have enough cores
datalad get . -J 4
```

You may also need to do:

```bash
datalad siblings add -d . \
  --name gin \
  --url git@gin.g-node.org:/fleischmann-lab/calimag-testdata.git
```

Then try to test whether you can push by making/editing a file in the `playground` folder:

```bash
echo "hello" >> playground/test.txt
```

However, if you're editing an existing file, you may need to unlock it first:

```bash
datalad unlock playground/test.txt
# or `datalad unlock .` for everything

```

Then to save (this will basically perform `git add/commit`, along with `git-annex` related things)

```bash
datalad save -m 'datalad: test push permisson' \
  --version-tag 'test' # optional, but may be useful
```

To push:

```bash
datalad push --to gin
# not tested, but the usual `git push` might already work
```

If you have a tag, `datalad push` does not seem to be able to push it, so you can do:

```bash
git push --tags # all tags, though the internet people don't recommend
# or
git push origin test # more recommended
```

##### Developing with updated data

The default GIN branch that the data are pulled from is `main`. Before testing, please remember to get the most recent data with `make get-data`.

If you are developing with updated data from a GIN branch, in `.env.test`, you can specify `GIN_DATALAD_CHECKOUT` to such GIN branch. Then run `make get-data` again to obtain the desired test data.

When your Gitlab branch is ready to merge, please open a pull request on GIN. After the GIN branch is merged, in `.env.test`, please change `GIN_DATALAD_CHECKOUT` back to `main` and push.

### Testing

The CI uses [GNU Make](https://www.gnu.org/software/make/) which can be installed by running the following commands:

- Linux: `sudo apt install make` (depends on the distribution/package manager)
- MacOS: `brew install make`, the command should then be available as `gmake` instead of `make` (https://formulae.brew.sh/formula/make)
- Windows: `choco install make` (https://community.chocolatey.org/packages/make)

Before testing, you will need updated data by running `make get-data`.

Check that all the tests are passing by running `make test`. If everything is green ✔, your development installation should be ready!

For environment variables related to testing, please see `.env.test`.

### Linting/Formatting

You can autoformat some files by running `make format` (need `prettier` from `npm` if using `make prettier-format`), and you can check for the linting CI job before pushing by running `make lint-python` or `make lint-all`.

### Precommit hooks

To activate the pre-commit hook, while `.venv` is activated, you can run `pre-commit install` once. As of now, the hook is only for lint checks. For commits that you do not want the hook to take effect, do `git commit --no-verify`.

### Environment variables

Currently, project-related environment variables for development are stored in `.env.test`:

| Environment variable   | Category   | Description                        |
| ---------------------- | ---------- | ---------------------------------- |
| `GIN_DATALAD_CHECKOUT` | data, test | GIN branch to get data for testing |

These are loaded automatically if you're using `make` tools. If you want to load them in your shell without using `make`, run the command `source .env.test`. To use them inside Python scripts, please use [`python-dotenv` API](https://pypi.org/project/python-dotenv/).

## Release

The `calimag` Conda package is automatically pushed to the [FleischmannLab channel on Anaconda.org](https://anaconda.org/FleischmannLab/calimag) when a new Git tag is pushed to the `master` branch. The list of all tags is available [here](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/tags).
For pre-releases on a development branch (i.e. release candidates with tag ending in `rc[X]`), the `windows` job needs to be triggered manually for the release job to be activated.

Gitlab CI needs to know the [Anaconda API token](https://anaconda.org/FleischmannLab/settings/access) to have the rights to push the newly built Conda package to [Anaconda.org](https://anaconda.org/). This token needs to be rotated (for now every year) and then copied as a [CI/CD variable to Gitlab](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/settings/ci_cd).

To build the package locally, you need to have [Mamba installed](https://mamba.readthedocs.io/en/latest/installation.html). Then run `conda mambabuild --channel conda-forge recipe/`. The newly built package will be located in your `<path/to/(mini)conda-directory>/conda-bld/` directory.
